package com.bank.india.bankjavaassessment.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.BeanUtils;

import com.bank.india.bankjavaassessment.dto.AccountDto;
import com.bank.india.bankjavaassessment.dto.CustomerDto;
import com.bank.india.bankjavaassessment.dto.UpdateCustomerDto;
import com.bank.india.bankjavaassessment.entity.Account;
import com.bank.india.bankjavaassessment.entity.Customer;
import com.bank.india.bankjavaassessment.exception.CustomerNotFoundException;
import com.bank.india.bankjavaassessment.exception.FirstNameNotAllowedException;
import com.bank.india.bankjavaassessment.exception.NeedValidGenderException;
import com.bank.india.bankjavaassessment.repository.CustomerRepository;
import com.bank.india.bankjavaassessment.service.CustomerServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CustomerServiceTest {

	@Mock
	ObjectMapper objectMapper;

	@InjectMocks
	CustomerServiceImpl customerServiceImpl;

	@Mock
	CustomerRepository customerRepository;

	public static final String customerId = "1234";
	AccountDto accountDto = null;
	CustomerDto customerDto = null;
	UpdateCustomerDto updateCustomerDto = null;
	Customer customer = null;
	Account account = null;
	List<AccountDto> accountsDto = null;
	List<Account> accounts=null;
	
	@Before
	public void setUp() {
		customer = new Customer();
		account = new Account();
		accountDto = new AccountDto();
		customerDto = new CustomerDto();
		updateCustomerDto = new UpdateCustomerDto();
		accounts = new ArrayList<>();
		accountsDto=new ArrayList<>();
		
		account.setId(1L);
		account.setAccountName("SA");
		account.setAccountBalance(24.76);
		account.setAccountNumber("1234");
		account.setBsb("344555");
		accounts.add(account);
		BeanUtils.copyProperties(account, accountDto);

		
		customer.setId(1L);
		customer.setCustomerId("1234");
		customer.setFirstName("sumanth");
		customer.setLastName("nettam");
		customer.setTitle("mr");
		customer.setGender("male");
		customer.setAccounts(accounts);
		accountsDto.add(accountDto);
		
		BeanUtils.copyProperties(customer, customerDto);
		customerDto.setAccounts(accountsDto);
		
		updateCustomerDto.setCustomerId("1234");
		updateCustomerDto.setFirstName("sumanth");
		updateCustomerDto.setLastName("nettam");
		updateCustomerDto.setGender("male");
		updateCustomerDto.setTitle("mr");
	}

	@Test
	public void testGetCustomerDetails() throws CustomerNotFoundException {
		Mockito.when(customerRepository.findByCustomerId(customerId)).thenReturn(Optional.of(customer));
		Mockito.when(objectMapper.convertValue(customer,CustomerDto.class)).thenReturn(customerDto);
        CustomerDto customerDto = customerServiceImpl.getCustomerDetails(customerId);
		assertEquals(customerDto.getCustomerId(), customer.getCustomerId());
	}
	
	@Test(expected = CustomerNotFoundException.class)
	public void testCustomerNotFound() throws CustomerNotFoundException
	{
		Mockito.when(customerRepository.findByCustomerId(customerId)).thenReturn(Optional.ofNullable(null));
		customerServiceImpl.getCustomerDetails(customerId);
	}
	
	
	@Test
	public void testUpdateCustomerDetails() throws CustomerNotFoundException, FirstNameNotAllowedException, NeedValidGenderException
	{
		Mockito.when(customerRepository.findByCustomerId(updateCustomerDto.getCustomerId())).thenReturn(Optional.of(customer));
		Mockito.when(objectMapper.convertValue(customer, CustomerDto.class)).thenReturn(customerDto);
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
	    CustomerDto customerDto = customerServiceImpl.updateCustomerDetails(updateCustomerDto);
	    assertEquals(customerDto.getFirstName(),updateCustomerDto.getFirstName());
	}
	
	@Test(expected = FirstNameNotAllowedException.class)
	public void testFirstNameNotAllowed() throws CustomerNotFoundException, FirstNameNotAllowedException, NeedValidGenderException
	{
		Mockito.when(customerRepository.findByCustomerId(updateCustomerDto.getCustomerId())).thenReturn(Optional.of(customer));
		updateCustomerDto.setFirstName("voldemort");
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
		Mockito.when(objectMapper.convertValue(customer, CustomerDto.class)).thenReturn(customerDto);
		customerServiceImpl.updateCustomerDetails(updateCustomerDto);
		
	}
	
	@Test(expected = NeedValidGenderException.class)
	public void testGenderAllowed() throws CustomerNotFoundException, FirstNameNotAllowedException, NeedValidGenderException
	{
		updateCustomerDto.setGender("maleee");
		customerServiceImpl.updateCustomerDetails(updateCustomerDto);
	}

}
