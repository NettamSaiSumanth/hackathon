package com.bank.india.bankjavaassessment.controller;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.bank.india.bankjavaassessment.dto.AccountDto;
import com.bank.india.bankjavaassessment.dto.CustomerDto;
import com.bank.india.bankjavaassessment.dto.GetCustomerIdDto;
import com.bank.india.bankjavaassessment.dto.UpdateCustomerDto;
import com.bank.india.bankjavaassessment.exception.CustomerNotFoundException;
import com.bank.india.bankjavaassessment.exception.FirstNameNotAllowedException;
import com.bank.india.bankjavaassessment.exception.NeedValidGenderException;
import com.bank.india.bankjavaassessment.exception.ServiceDownException;
import com.bank.india.bankjavaassessment.service.CustomerService;

@RunWith(MockitoJUnitRunner.class)
public class CustomerControllerTest {

	@InjectMocks
	private CustomerController customerController;
	
	@Mock
	private CustomerService customerService;
	
	AccountDto accountDto = null;
	CustomerDto customerDto = null;
	UpdateCustomerDto updateCustomerDto = null;
	GetCustomerIdDto getCustomerIdDto = null;
	List<AccountDto> accountList = null;

	@Before
	public void before() {
		accountDto = new AccountDto();
		customerDto = new CustomerDto();
		updateCustomerDto = new UpdateCustomerDto();
		getCustomerIdDto = new GetCustomerIdDto();
		
		getCustomerIdDto.setCustomerId("12345");
		
		accountDto.setAccountName("SA");
		accountDto.setAccountBalance(24.76);
		accountDto.setAccountNumber("1234");
		accountDto.setBsb("344555");

		accountList = new ArrayList<>();
		accountList.add(accountDto);
		customerDto.setCustomerId("12345");
		customerDto.setAccounts(accountList);
		customerDto.setFirstName("sumanth");
		customerDto.setLastName("nettam");
		customerDto.setTitle("mr");

		updateCustomerDto.setCustomerId("1234");
		updateCustomerDto.setFirstName("sumanth");
		updateCustomerDto.setLastName("nettam");
		updateCustomerDto.setGender("male");
		updateCustomerDto.setTitle("mr");

	}

	@Test
	public void testGetCustomer() throws CustomerNotFoundException {
		Mockito.when(customerService.getCustomerDetails(getCustomerIdDto.getCustomerId())).thenReturn(customerDto);
		ResponseEntity<CustomerDto> response = customerController.getCustomer(getCustomerIdDto);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test()
	public void testUpdateCustomer() throws CustomerNotFoundException, FirstNameNotAllowedException, NeedValidGenderException {
		Mockito.when(customerService.updateCustomerDetails(updateCustomerDto)).thenReturn(customerDto);
		ResponseEntity<CustomerDto> response = customerController.updateCustomer(updateCustomerDto);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
	}

	@Test(expected = ServiceDownException.class)
	public void testGetCustomerDetailsServiceDown() throws ServiceDownException {
		customerController.getCustomerDetailsServiceDown(getCustomerIdDto);
	}

	@Test(expected = ServiceDownException.class)
	public void testUpdateCustomerDetailsServiceDown() throws ServiceDownException {
		customerController.updateCustomerDetailsServiceDown(updateCustomerDto);
	}
}
