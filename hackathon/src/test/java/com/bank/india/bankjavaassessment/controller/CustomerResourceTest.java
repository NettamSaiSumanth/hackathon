package com.bank.india.bankjavaassessment.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import com.bank.india.bankjavaassessment.dto.GetCustomerIdDto;
import com.bank.india.bankjavaassessment.dto.UpdateCustomerDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CustomerResourceTest {

	@InjectMocks
	CustomerController customerController;

	@Autowired
	MockMvc mockMvc;

	UpdateCustomerDto updateCustomerDto = null;
	GetCustomerIdDto getCustomerIdDto = null;

	public static final String GET_CUSTOMER_DETAILS_URL = "/customers/getcustomerdetails/{customerId}";
	public static final String UPDATE_CUSTOMER_DETAILS_URL = "/customers/updatecustomerdetails";
	public static final int ERROR_CODE = 400;

	public static String convertToJson(UpdateCustomerDto updateCustomerDto) throws JsonProcessingException {
		return new ObjectMapper().writeValueAsString(updateCustomerDto);
	}

	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
		updateCustomerDto = new UpdateCustomerDto();
		getCustomerIdDto = new GetCustomerIdDto();

		getCustomerIdDto.setCustomerId("12345a");

		updateCustomerDto.setCustomerId("1234a");
		updateCustomerDto.setFirstName("sumanth");
		updateCustomerDto.setLastName("nettam");
		updateCustomerDto.setGender("male");
		updateCustomerDto.setTitle("mr");

	}

	@Test
	@WithMockUser(username = "userrole", password = "userpassword", roles = "USER")
	public void testGetCustomerIdInvalid() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get(GET_CUSTOMER_DETAILS_URL, getCustomerIdDto.getCustomerId()))
				.andExpect(MockMvcResultMatchers.status().is(ERROR_CODE));
	}

	@Test
	public void testUpdateCustomer() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put(UPDATE_CUSTOMER_DETAILS_URL)
				.with(SecurityMockMvcRequestPostProcessors.user("adminrole").password("adminpassword").roles("ADMIN"))
				.with(SecurityMockMvcRequestPostProcessors.csrf()).content(convertToJson(updateCustomerDto))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().is(ERROR_CODE));
	}

}
