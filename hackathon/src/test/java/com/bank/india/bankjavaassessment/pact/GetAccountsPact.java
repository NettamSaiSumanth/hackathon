package com.bank.india.bankjavaassessment.pact;


import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;

import com.bank.india.bankjavaassessment.BankJavaAssessmentApplication;

import au.com.dius.pact.provider.junit.Provider;
import au.com.dius.pact.provider.junit.State;
import au.com.dius.pact.provider.junit.loader.PactFolder;
import au.com.dius.pact.provider.junit.target.HttpTarget;
import au.com.dius.pact.provider.junit.target.Target;
import au.com.dius.pact.provider.junit.target.TestTarget;
import au.com.dius.pact.provider.spring.SpringRestPactRunner;

//import au.com.dius.pact.provider.junit.Provider;
//import au.com.dius.pact.provider.junit.State;
//import au.com.dius.pact.provider.junit.loader.PactFolder;
//import au.com.dius.pact.provider.junit.target.HttpTarget;
//import au.com.dius.pact.provider.junit.target.Target;
//import au.com.dius.pact.provider.junit.target.TestTarget;
//import au.com.dius.pact.provider.spring.SpringRestPactRunner;

@RunWith(SpringRestPactRunner.class)
@SpringBootTest(classes = BankJavaAssessmentApplication.class,properties= {"spring.profiles.active = test","spring.cloud.config.enabled = false"}
                    ,webEnvironment=SpringBootTest.WebEnvironment.DEFINED_PORT)
@PactFolder("pacts/getcustomerdetails")
@Provider("getcustomerdetails_provider")
public class GetAccountsPact {

	@TestTarget
	public final Target target = new HttpTarget("http","localhost",8085,"/api");

	@State("get customer details running")
	public void testGetStates()
	{
		
	}
}
