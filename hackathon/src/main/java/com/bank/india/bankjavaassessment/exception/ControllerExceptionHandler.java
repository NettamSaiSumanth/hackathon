package com.bank.india.bankjavaassessment.exception;

import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.bank.india.bankjavaassessment.dto.ErrorResponseDto;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(CustomerNotFoundException.class)
	public ResponseEntity<String> customerNotFound(CustomerNotFoundException customerNotFoundException) {
		log.warn(customerNotFoundException.getMessage());
		return new ResponseEntity<String>(customerNotFoundException.getMessage(), HttpStatus.NOT_FOUND);

	}
	@ExceptionHandler(NeedValidGenderException.class)
	public ResponseEntity<String> needValidGender(NeedValidGenderException needValidGenderException) {
		log.warn(needValidGenderException.getMessage());
		return new ResponseEntity<String>(needValidGenderException.getMessage(), HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(FirstNameNotAllowedException.class)
	public ResponseEntity<String> firstNameIsNotAllowed(FirstNameNotAllowedException firstNameNotAllowedException) {
		log.warn(firstNameNotAllowedException.getMessage());
		return new ResponseEntity<String>(firstNameNotAllowedException.getMessage(), HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(ServiceDownException.class)
	public ResponseEntity<String> serviceWasDown(ServiceDownException serviceDownException) {
		log.warn(serviceDownException.getMessage());
		return new ResponseEntity<String>(serviceDownException.getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
	}

	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<String> errorDetails = new ArrayList<>();
		for (ObjectError error : exception.getBindingResult().getAllErrors()) {
			errorDetails.add(error.getDefaultMessage());
			log.warn(error.getDefaultMessage());
		}
		ErrorResponseDto errorResponse = new ErrorResponseDto("Validation Failed", errorDetails);
		return new ResponseEntity<Object>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	@Override
	protected ResponseEntity<Object> handleBindException(BindException bindingException, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		List<String> errorDetails = new ArrayList<>();
		for (ObjectError error : bindingException.getBindingResult().getAllErrors()) {
			errorDetails.add(error.getDefaultMessage());
			log.warn(error.getDefaultMessage());
		}
		ErrorResponseDto errorResponse = new ErrorResponseDto("Validation Failed", errorDetails);
		return new ResponseEntity<Object>(errorResponse, HttpStatus.BAD_REQUEST);

	}

}
