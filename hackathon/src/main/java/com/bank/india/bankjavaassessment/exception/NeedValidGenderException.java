package com.bank.india.bankjavaassessment.exception;

public class NeedValidGenderException extends Exception {

	private static final long serialVersionUID = 1L;

	public NeedValidGenderException(String message) {
		super(message);
	}

}
