package com.bank.india.bankjavaassessment.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "CUSTOMERS")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Customer implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "CUSTOMER_ID", unique = true)
	private String customerId;

	@Column(name = "TITLE")
	private String title;

	@Column(name = "CUST_FIRST_NAME")
	private String firstName;

	@Column(name = "CUST_LAST_NAME")
	private String lastName;

	@Column(name = "CUST_GENDER")
	private String gender;

	@OneToMany
	private List<Account> accounts;
}
