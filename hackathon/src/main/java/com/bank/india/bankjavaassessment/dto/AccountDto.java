package com.bank.india.bankjavaassessment.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AccountDto {

	private String accountNumber;
	private String bsb;
	private String accountName;
	private double accountBalance;
}
