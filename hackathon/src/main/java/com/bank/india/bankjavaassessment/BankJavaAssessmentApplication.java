package com.bank.india.bankjavaassessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

@SpringBootApplication
@EnableHystrix
public class BankJavaAssessmentApplication {
	public static void main(String[] args) {
		SpringApplication.run(BankJavaAssessmentApplication.class, args);
	}

}
