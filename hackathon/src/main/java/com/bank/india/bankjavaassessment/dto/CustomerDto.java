package com.bank.india.bankjavaassessment.dto;


import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class CustomerDto {

	private String customerId;
	private String title;
	private String firstName;
	private String lastName;
	private String gender;
	private List<AccountDto> accounts;
	
	
}
