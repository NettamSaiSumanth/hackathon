package com.bank.india.bankjavaassessment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bank.india.bankjavaassessment.constannts.ApiConstant;
import com.bank.india.bankjavaassessment.dto.CustomerDto;
import com.bank.india.bankjavaassessment.dto.UpdateCustomerDto;
import com.bank.india.bankjavaassessment.entity.Customer;
import com.bank.india.bankjavaassessment.exception.CustomerNotFoundException;
import com.bank.india.bankjavaassessment.exception.FirstNameNotAllowedException;
import com.bank.india.bankjavaassessment.exception.NeedValidGenderException;
import com.bank.india.bankjavaassessment.repository.CustomerRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	CustomerRepository customerRepository;

	@Transactional
	public CustomerDto getCustomerDetails(String customerId) throws CustomerNotFoundException {
		log.info("In Customer Service Finding Customer");
		Customer customer = customerRepository.findByCustomerId(customerId).orElse(null);
		if (customer == null) {
			throw new CustomerNotFoundException(String.format(ApiConstant.CUSTOMER_NOT_FOUND, customerId));
		}

		CustomerDto customerDto = objectMapper.convertValue(customer, CustomerDto.class);
		return customerDto;
	}

	@Override
	@Transactional(rollbackFor = FirstNameNotAllowedException.class)
	public CustomerDto updateCustomerDetails(UpdateCustomerDto updateCustomerDto)
			throws CustomerNotFoundException, FirstNameNotAllowedException, NeedValidGenderException {
		log.info("In Customer Service Finding Customer");
		if(
		   (updateCustomerDto.getGender().equalsIgnoreCase("male")) || 
		   (updateCustomerDto.getGender().equalsIgnoreCase("female")) ||
		   (updateCustomerDto.getGender().equalsIgnoreCase("others")) 
		  )
		{	
		Customer customer = customerRepository.findByCustomerId(updateCustomerDto.getCustomerId()).orElse(null);
		if (customer == null) {
			throw new CustomerNotFoundException(
					String.format(ApiConstant.CUSTOMER_NOT_FOUND, updateCustomerDto.getCustomerId()));
		}

		customer.setFirstName(updateCustomerDto.getFirstName());
		customer.setLastName(updateCustomerDto.getLastName());
		customer.setGender(updateCustomerDto.getGender());
		customer.setTitle(updateCustomerDto.getTitle());
		Customer customerDetails = customerRepository.save(customer);
		log.info("Updated Customer Details");
		if (customerDetails.getFirstName().equalsIgnoreCase("voldemort")) {
			throw new FirstNameNotAllowedException(ApiConstant.FIRST_NAME_NOT_ALLOWED);
		}
		CustomerDto customerDto = objectMapper.convertValue(customerDetails, CustomerDto.class);
		return customerDto;
		}
		else
		{
			throw new NeedValidGenderException(ApiConstant.CUST_VALID_GENDER);
		}

	}
}
