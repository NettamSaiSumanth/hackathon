package com.bank.india.bankjavaassessment.exception;

public class ServiceDownException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public ServiceDownException(String message) {
		super(message);
		
	}

}
