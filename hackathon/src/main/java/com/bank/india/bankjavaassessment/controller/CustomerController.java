package com.bank.india.bankjavaassessment.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.india.bankjavaassessment.constannts.ApiConstant;
import com.bank.india.bankjavaassessment.dto.CustomerDto;
import com.bank.india.bankjavaassessment.dto.GetCustomerIdDto;
import com.bank.india.bankjavaassessment.dto.UpdateCustomerDto;
import com.bank.india.bankjavaassessment.exception.CustomerNotFoundException;
import com.bank.india.bankjavaassessment.exception.FirstNameNotAllowedException;
import com.bank.india.bankjavaassessment.exception.NeedValidGenderException;
import com.bank.india.bankjavaassessment.exception.ServiceDownException;
import com.bank.india.bankjavaassessment.service.CustomerService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/customers")
public class CustomerController {

	@Autowired
	CustomerService customerService;

	@GetMapping("/getcustomerdetails/{customerId}")
//	@HystrixCommand(fallbackMethod = "getCustomerDetailsServiceDown", ignoreExceptions = {
//			CustomerNotFoundException.class })
	public ResponseEntity<CustomerDto> getCustomer(@Valid GetCustomerIdDto getCustomerIdDto)
			throws CustomerNotFoundException {
		log.info("In Customer Controller Getting customer Details By ID");
		CustomerDto customerDto = customerService.getCustomerDetails(getCustomerIdDto.getCustomerId());
		log.info("Returning Customer Details ");
		return new ResponseEntity<>(customerDto, HttpStatus.OK);
	}

	@PutMapping("/updatecustomerdetails")
	@HystrixCommand(fallbackMethod = "updateCustomerDetailsServiceDown", ignoreExceptions = {
			CustomerNotFoundException.class, FirstNameNotAllowedException.class, NeedValidGenderException.class })
	public ResponseEntity<CustomerDto> updateCustomer(@Valid @RequestBody UpdateCustomerDto updateCustomerDto)
			throws CustomerNotFoundException, FirstNameNotAllowedException, NeedValidGenderException {
		log.info("Updating Customer Details");
		CustomerDto customerDto = customerService.updateCustomerDetails(updateCustomerDto);
		log.info("Returning Customer Details Updated ");
		return new ResponseEntity<>(customerDto, HttpStatus.CREATED);
	}

	ResponseEntity<CustomerDto> getCustomerDetailsServiceDown(GetCustomerIdDto getCustomerIdDto) throws ServiceDownException {
		log.warn("get Customer Details API was Failed");
		throw new ServiceDownException(ApiConstant.GET_DETAILS_SERVICE_DOWN);
	}

	ResponseEntity<CustomerDto> updateCustomerDetailsServiceDown(UpdateCustomerDto updateCustomerDto)
			throws ServiceDownException {
		log.warn("Update Customer Details Was Failed");
		throw new ServiceDownException(ApiConstant.UPDATE_DETAILS_SERVICE_DOWN);
	}

}
