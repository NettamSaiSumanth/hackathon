package com.bank.india.bankjavaassessment.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import com.bank.india.bankjavaassessment.constannts.ApiConstant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetCustomerIdDto {

	
	@NotEmpty(message = ApiConstant.CUST_ID_NOT_NULL)
	@Pattern(regexp="^[0-9]*$",message = ApiConstant.CUST_ID_MIN_ONE)
	private String customerId;
}
