package com.bank.india.bankjavaassessment.dto;

import java.util.List;

public class ErrorResponseDto {

	public String errorMessage;
	public List<String> errorDetails;
	public ErrorResponseDto() {
		super();
	}
	public ErrorResponseDto(String errorMessage, List<String> errorDetails) {
		super();
		this.errorMessage = errorMessage;
		this.errorDetails = errorDetails;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public List<String> getErrorDetails() {
		return errorDetails;
	}
	public void setErrorDetails(List<String> errorDetails) {
		this.errorDetails = errorDetails;
	}
	
}
