package com.bank.india.bankjavaassessment.constannts;

public class ApiConstant {

	public static final String CUSTOMER_NOT_FOUND = "Customer Not Found with this customer ID %s";
	public static final String GET_DETAILS_SERVICE_DOWN = "Unable to Get Details Please Contact +45644";
	public static final String UPDATE_DETAILS_SERVICE_DOWN = "Unable to Update Details Please Contact +45644";

	public static final String CUST_ID_MIN_ONE = "customerId should be number";
	public static final String CUST_ID_NOT_NULL = "customerId should not be null";

	public static final String GENDER_NOT_EMPTY = "Gender Should not be empty";
	public static final String FIRST_NAME_NOT_EMPTY = "First Name Should not be empty";
	public static final String LAST_NAME_NOT_EMPTY = "Last Name Should not be empty";
	public static final String TITLE_NOT_EMPTY = "Title Should not be empty";
	public static final String CUST_NOT_NUMBER = "Customer Id Should be a number";
	public static final String CUST_VALID_GENDER = "Enter Valid Gender";
 
	public static final String FIRST_NAME_NOT_ALLOWED = "Voldemort Name is not allowed as First name";

}
