package com.bank.india.bankjavaassessment.exception;

public class FirstNameNotAllowedException extends Exception {

	private static final long serialVersionUID = 1L;

	public FirstNameNotAllowedException(String message) {
		super(message);

	}

}
