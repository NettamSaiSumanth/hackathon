package com.bank.india.bankjavaassessment.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "ACCOUNTS")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Account implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "ACCOUNT_NUMBER", unique = true)
	private String accountNumber;

	@Column(name = "BSB")
	private String bsb;

	@Column(name = "ACCOUNT_TYPE")
	private String accountName;

	@Column(name = "ACCOUNT_BALANCE")
	private double accountBalance;

}
