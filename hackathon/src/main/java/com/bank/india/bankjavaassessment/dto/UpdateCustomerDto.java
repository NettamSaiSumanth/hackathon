package com.bank.india.bankjavaassessment.dto;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import com.bank.india.bankjavaassessment.constannts.ApiConstant;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UpdateCustomerDto {
	
	@NotEmpty(message = ApiConstant.CUST_ID_NOT_NULL)
	@Pattern(regexp="^[0-9]*$",message = ApiConstant.CUST_ID_MIN_ONE)
	private String customerId;
	
	@NotEmpty(message=ApiConstant.TITLE_NOT_EMPTY)
	private String title;
	@NotEmpty(message = ApiConstant.FIRST_NAME_NOT_EMPTY)
	private String firstName;
	@NotEmpty(message = ApiConstant.LAST_NAME_NOT_EMPTY)
	private String lastName;
	@NotEmpty(message = ApiConstant.GENDER_NOT_EMPTY)
	private String gender;
}
