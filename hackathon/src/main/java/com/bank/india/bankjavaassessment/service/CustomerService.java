package com.bank.india.bankjavaassessment.service;

import com.bank.india.bankjavaassessment.dto.CustomerDto;
import com.bank.india.bankjavaassessment.dto.UpdateCustomerDto;
import com.bank.india.bankjavaassessment.exception.CustomerNotFoundException;
import com.bank.india.bankjavaassessment.exception.FirstNameNotAllowedException;
import com.bank.india.bankjavaassessment.exception.NeedValidGenderException;

public interface CustomerService {
	
	CustomerDto getCustomerDetails(String customerId) throws CustomerNotFoundException;	
	CustomerDto updateCustomerDetails(UpdateCustomerDto updateCustomerDto) throws CustomerNotFoundException, FirstNameNotAllowedException, NeedValidGenderException;

}
