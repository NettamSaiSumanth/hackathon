Main Objective Of this project is to get customer details with accounts and to update customer details

Controller:
Created 2 API'S 1)getcustomerdetails ---> Given customerID as input from URI and validated whether it is number or not.
                2)updatecustomerdetails --->  Given Customer Details as input and validated details.

Service:
For getcustomerdetails retrieved details from repository and sent to controller, if not found exception is thrown
For updateCustomerDetails 1)If Gender is either male or female or others then further steps are allowed else exception is thrown.
                          2)If customer Id is not found exception is thrown.
                          3) Customer Details are updated and customer object is returned
                          4)To check rollback is working if returned customer object customername is "voldemort" exception thrown So saved customer is rollbacked. 	

Repository:
Used JPA Repository

Exception:
Created custom exceptions and handled validation and customer exceptions through controller advice.

Configuration
added swagger configuration
added Spring Security for Basic API authentication and  authorization.

Constants:
constants are in constant folder

Dto:
Dto's are used to get requests and to send responses

Test:
Unit Testing
Tests are covered for all methods in controller,service,security.

			  
						  
